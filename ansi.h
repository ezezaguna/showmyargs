#ifndef ansi_h_INCLUDED
#define ansi_h_INCLUDED

#include <string>

using namespace std;

constexpr const char* ANSI_COLOR[] {"\x1b[30m", "\x1b[31m",
	"\x1b[32m", "\x1b[33m","\x1b[34m","\x1b[35m","\x1b[36m","\x1b[37m"};

constexpr auto ANSI_BOLD    = "\x1b[1m";
constexpr auto ANSI_REVERSE = "\x1b[7m";

constexpr auto ANSI_RESET   = "\x1b[0m";


#endif // ansi_h_INCLUDED

