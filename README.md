# Show My Args

A shell script and C++ program to nicely print command line arguments.

I've made this little program for my friends whom I plan teaching Linux. It's
very useful for demonstrating things like quotes around an argument, escaping
characters, command substitution, globbing, et cetera...

## Shell script version

The shell script doesn't require any setup, just download and run it.

### Installing

```sh
wget 'https://gitlab.com/ezezaguna/showmyargs/-/raw/master/showmyargs.sh' -O ~/.local/bin/showmyargs.sh
chmod +x ~/.local/bin/showmyargs.sh
```

### Running

```sh
~/.local/bin/showmyargs.sh "he"'llo,' these\ are $(echo my arguments)
```

If `~/.local/bin` is in your `$PATH` environment variable, you can run it without specifying
the full path of the script.

```sh
showmyargs.sh "he"'llo,' these\ are $(echo my arguments)
```

## C++ version 

Has the same functionality as the shell script.

### Building

Requires a C++ compiler and `make`

```sh
make
```

## License

[Unlicense](https://gitlab.com/ezezaguna/showmyargs/-/blob/master/LICENSE)




