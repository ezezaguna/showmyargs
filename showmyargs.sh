#!/bin/sh
# showmyargs.sh by ezezaguna

#if [ $# -le 0 ]; then
#	echo "No arguments given." 1>&2
#	exit 1
#fi

if [ -t 1 ]; then # are outputting to a terminal?
	_BOLD=$(tput bold)
	_REVERSE=$(tput rev)
	_RESET=$(tput sgr0)

	# POSIX shell does not have arrays
	for i in $(seq 1 7); do
		eval _COLOR_${i}=$(tput setaf $i)
	done
else
	_BOLD=""
	_REVERSE=""
	_RESET=""

	# POSIX shell does not have arrays
	for i in $(seq 1 7); do
		eval _COLOR_${i}=""
	done
fi

# print 0 argument in gray
echo "${_BOLD}${_COLOR_7}0 argument: ${_REVERSE}$0${_RESET}"

# print all the rest in rainbow
for i in $(seq 1 $#); do
	color=$(expr $i % 6 + 1)
	eval echo "\"${_BOLD}\${_COLOR_$(expr $i % 6 + 1)}$i argument: ${_REVERSE}\$${i}${_RESET}\""
done
