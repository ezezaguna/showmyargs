/* showmyargs by ezezaguna */

#include <iostream>
#include <unistd.h> // for isatty() and fileno()
#include "ansi.h"

using namespace std;

int main(int argc, char const *argv[]) {

	// exit if no arguments
	// actually it's kinda impossible, if the program is executed from a shell
	if (argc == 0) {
		cerr << "No arguments given." << endl;
		return 1;
	}

	if (isatty(fileno(stdout))) {

		// print 0 argument in gray
		cout << ANSI_BOLD << ANSI_COLOR[7] << " argument: "
		     << ANSI_REVERSE << argv[0] << ANSI_RESET << "\n";

		// print all the rest in rainbow
		for (auto i=1; i<argc; ++i) {
			cout << ANSI_BOLD << ANSI_COLOR[i % 6 + 1] << i << " argument: " 
			     << ANSI_REVERSE << argv[i] << ANSI_RESET << "\n";
		}

	} else {
		// print all arguments
		for (auto i=0; i<argc; ++i) {
			cout << i << " argument: " << argv[i] << "\n";
		}
	}

	cout << flush;

	return 0;
}
